#!/bin/bash

if [ -z ${1+x} ]; then
	sudo docker exec --user root -it mongodb bash 
else
	sudo docker exec --user root mongodb bash -c "$1"
fi

