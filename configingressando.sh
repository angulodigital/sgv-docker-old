sk for the user password
# Script only works if sudo caches the password for a few minutes
sudo true
	
# Prevent sudo timeout
sudo -v # ask for sudo password up-front
while true; do
	# Update user's timestamp without running a command
	sudo -nv; sleep 1m
	# Exit when the parent process is not running any more. In fact this loop
	# would be killed anyway after being an orphan(when the parent process
	# exits). But this ensures that and probably exit sooner.
	kill -0 $$ 2>/dev/null || exit
done &	

if [ -e "/etc/init.d/glassfish" ]; then
	echo "Parando/desativando servico local : glassfish"
	sudo service glassfish stop;
	sudo update-rc.d glassfish disable;
fi

if [ -e "/etc/init.d/postgresql" ]; then
	echo "Parando/desativando servico local : postgresql"
	sudo service postgresql stop;
	sudo update-rc.d postgresql disable;
fi

if [ -e "/etc/init.d/mongodb" ]; then
	echo "Parando/desativando servico local : mongodb"
	sudo service mongodb stop;
	sudo update-rc.d mongodb disable;
fi

# Install kernel extra's to enable docker aufs support
sudo apt-get -y install linux-image-extra-$(uname -r)

# Add Docker PPA and install latest version
# sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
# sudo sh -c "echo deb https://get.docker.io/ubuntu docker main > /etc/apt/sources.list.d/docker.list"
# sudo apt-get update
# sudo apt-get install lxc-docker -y

# removing old docker 
sudo apt-gsudo move docker* -y  
sudo apt-get autoremove -y
sudo apt-get autoclean -y

# install git
sudo apt-get install git curl wget -y

# Alternatively you can use the official docker install script
wget --progress=bar:force:noscroll -qO- https://get.docker.com/ | sudo sh

#Install docker-compose
COMPOSE_VERSION=`git ls-remote https://github.com/docker/compose | grep refs/tags | grep -oP "[0-9]+\.[0-9][0-9]+\.[0-9]+$" | tail -n 1`
sudo sh -c "curl -L https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose"
sudo chmod +x /usr/local/bin/docker-compose
sudo sh -c "curl -L https://raw.githubusercontent.com/docker/compose/${COMPOSE_VERSION}/contrib/completion/bash/docker-compose > /etc/bash_completion.d/docker-compose"

# Install docker-cleanup command
cd /tmp
sudo git clone https://gist.github.com/76b450a0c986e576e98b.git
cd 76b450a0c986e576e98b
sudo git fetch && git pull
sudo mv docker-cleanup /usr/local/bin/docker-cleanup
sudo chmod +x /usr/local/bin/docker-cleanup

# Install ctop
sudo wget --progress=bar:force:noscroll https://github.com/bcicen/ctop/releases/download/v0.5/ctop-0.5-linux-amd64 -O /usr/local/bin/ctop
sudo chmod +x /usr/local/bin/ctop

sudo docker-compose down
sudo docker volume remove sgvdocker_db-data
sudo docker-cleanup
sudo docker volume prune -f
sudo docker container prune -f
sudo docker container rm tomcat -f
sudo docker container rm postgres -f

# remove webapps antigas
sudo rm -Rf /java/tomcat/webapps
			
sudo docker-compose build --no-cache
	
sudo docker-compose up -d
