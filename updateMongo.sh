#!/bin/bash

./showMongo.sh "
				echo 'Atualizando arquivo alo.zip no mongodb'; \
				wget --timeout 3600 --progress=bar:force:noscroll https://storage.googleapis.com/sga-file/sgv/new/alo.zip -O /tmp/alo.zip; \
				cd tmp; \
				mongofiles --verbose --replace --db configs delete alo.zip; \
				mongofiles --verbose --replace --db configs put alo.zip; \
				mongofiles --db configs search alo.zip"